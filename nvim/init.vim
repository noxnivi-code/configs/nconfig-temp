
" initialize plugin system Vim-Plug
" we set a different path to have everything inside the 'nvim' folder
call plug#begin('~/.config/nvim/plugged')


" file manager NerdTree
Plug 'scrooloose/nerdtree'

" Icons
Plug 'ryanoasis/vim-devicons'

" Nerd Fonts
Plug 'ryanoasis/nerd-fonts'

" airline > status line at bottom
Plug 'vim-airline/vim-airline'

" indent lines
Plug 'nathanaelkane/vim-indent-guides'

" auto pair () [] {}
Plug 'jiangmiao/auto-pairs'

" Git integration
Plug 'tpope/vim-fugitive'

" Codedark theme
Plug 'tomasiser/vim-code-dark'

call plug#end()
" end Vim-Plug plugins

" base config theming
colorscheme codedark


" autocommands
autocmd VimEnter * NERDTree | wincmd p



" let commands
" set NerdTree at right of window
let g:NERDTreeWinPos = "right"

let g:airline_theme = 'codedark'


" set commands
set number			      " adds line numbers
set cursorline 			" highlight current line
set cursorcolumn        " highlight current column

set autoindent 			" indent lines automatically
set smartindent	 		" improve indentation for languages

set expandtab           " converts tabs to spaces
set shiftwidth=3		   " sets number of spaces with shift
set tabstop=3			   " sets number spaces for tab


